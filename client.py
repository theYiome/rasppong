import serial
import time
import plot
import matplotlib.pyplot as plt

name = input("Enter your nickname: ")

# change ACM number as found from ls /dev/tty/ACM*
port = serial.Serial("/dev/ttyACM0", 9600)
print("Sending reset request to reset the game...")
port.write(bytes(b'reset'))

print("Hello {}, you can start playing now!".format(name))

plt.ion()
plt.show()

while True:
    try:
        score = port.readline()
        wynik = int(score)
        
        filename = "{}.txt".format(name)
        f = open(filename, "a+")
        f.write(str(wynik) + "\n")
        print("{} appended to file!".format(wynik))
        f.close()

        plot.plot_file(filename, "Wynik gracza {}".format(name))
    except KeyboardInterrupt as e:
        print("Thanks for playing!")
        break
    
