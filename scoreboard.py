# libraries
import os
import matplotlib.pyplot as plt

# Data
out = os.listdir('.')

file_names = []
for x in out:
    if '.txt' in x:
        file_names.append(x)

results = dict()
for x in file_names:
    tmp = []
    
    f = open(x, "r")
    line = f.readline()
    while line != '':
        tmp.append(int(line))
        line = f.readline()
    f.close()

    results[x.split(".")[0]] = tmp


legend = []
for key, value in results.items():
    print(key)
    print(value)
    plt.plot(value)
    legend.append(key)

plt.ylabel("Wyniki graczy")
plt.legend(legend, loc='upper left')
plt.show()