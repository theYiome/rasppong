#include <TFT.h> // Hardware-specific library
#include <SPI.h>

#define CS   10
#define DC   9
#define RESET  8 
int pressureAnalogPin = 0; //pin where our pressure pad is located.
int pressureReading; //variable for storing our reading

//Adjust these if required.
int noPressure = 100; //max value for no pressure on the pad
int lightPressure = 450; //max value for light pressure on the pad 

// pin definition for the Leonardo
// #define CS   7
// #define DC   0
// #define RESET  1 

TFT myScreen = TFT(CS, DC, RESET);

// variable to keep track of the elapsed time
int counter = 0;
const char * last = 0;

void setup(){
  Serial.begin(9600);
  myScreen.begin();  
  myScreen.background(0,0,0); // clear the screen
  // increase font size for text in loop() 
  myScreen.setTextSize(3);
  myScreen.noFill();
}

float playerPosition = 49.0;
float playerSpeed = 2.618;
float playerSide = 30;

float computerPosition = 49.0;

float xBallSpeed = 5.8;
float yBallSpeed = 1.218;
float xBallPosition = 80.0;
float yBallPosition = 64.0;

boolean reset = false;
int points = 0;

// serial port data container
String data = "";

void loop(){
    // CLEAR OLD STUFF
    myScreen.stroke(0, 0, 0);
    myScreen.rect(0, playerPosition, 5, 30);
    myScreen.rect(155, computerPosition, 5, 30);
    myScreen.circle(xBallPosition, yBallPosition, 5);

    // LOGIC
    pressureReading = analogRead(pressureAnalogPin);
    if(pressureReading < noPressure) {
      // Do nothing
      ;
    }
    else if(pressureReading < lightPressure) {
        playerPosition = playerPosition - playerSpeed;
        if(playerPosition < 0) playerPosition = 0;
    }
    else {
      playerPosition = playerPosition + playerSpeed;
      if(playerPosition > 128 - playerSide) playerPosition = 128 - playerSide;
    }

    xBallPosition = xBallPosition + xBallSpeed;
    yBallPosition = yBallPosition + yBallSpeed;

    if(xBallPosition > 150) {
      xBallPosition = 150;
      xBallSpeed = -xBallSpeed;
    }

    // kolizja z padem
    if(xBallPosition < 10) {
      if(yBallPosition <= playerPosition + playerSide + 5 && yBallPosition >= playerPosition - 5) {
        // odbijamy
        xBallPosition = 10;
        points = points + 1;
      } else {
        // nie udalo sie odbic, resetujemy
        reset = true;
      }
      
      xBallSpeed = -xBallSpeed;
    }

    if(yBallPosition > 123) {
      yBallPosition = 123;
      yBallSpeed = -yBallSpeed;
    }

    if(yBallPosition < 5) {
      yBallPosition = 5;
      yBallSpeed = -yBallSpeed;
    }

    computerPosition = yBallPosition - playerSide/2.0;
    if(computerPosition > 128 - playerSide) computerPosition = 128 - playerSide;
    if(computerPosition < 0) computerPosition = 0;

    // DRAW NEW STUFF
    // set drawing color to white
    myScreen.stroke(255, 255, 255);
    // draw player pad
    myScreen.rect(0, playerPosition, 5, 30);
    // draw ball
    myScreen.circle(xBallPosition, yBallPosition, 5);
    // draw computer pad
    myScreen.rect(155, computerPosition, 5, 30);
    delay(25);

    // check if python client requested game restart
    if(Serial.available() > 0) {
      data = Serial.readString();
      if(data == "reset") reset = true;
    }

    if(reset) {
      reset = false;
      xBallPosition = 80.0;
      yBallPosition = 64.0;
      playerPosition = 49.0;
      
      myScreen.setTextSize(2);
      
      myScreen.stroke(255,255,255);
      char tmp[32];
      String s = "Score: " + String(points);
      s.toCharArray(tmp, 32);
      myScreen.text(tmp, 20, 20);

      Serial.println(points, DEC);
      delay(3000);
      myScreen.background(0, 0, 0);
      points = 0;
    }

}
