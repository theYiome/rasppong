import matplotlib.pyplot as plt
import numpy as np

def plot_file(filename, ylabel):
    f = open(filename, "r")
    array = []
    line = f.readline()

    while line != '':
        array.append(int(line))
        line = f.readline()

    plt.xticks(np.arange(0, len(array) + 1, 1.0))
    plt.plot(array)
    plt.ylabel(ylabel)
    plt.draw()
    plt.pause(0.001)